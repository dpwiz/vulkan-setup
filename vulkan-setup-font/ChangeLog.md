# Changelog for vulkan-setup-font

## Unreleased changes

## 0.2.1.1

* Fix origin offsets in `putLine`.

## 0.2.1.0

* Fix `putLine` to use Vulkan negative-y up.
* Fix `putLine` annotation for size and offset.

## 0.2.0.0

Initial import (extracted from `playground3`).
