{-# LANGUAGE AllowAmbiguousTypes #-}

module Vulkan.Setup.Render.Texture
  ( Texture(..)
  , destroy

  , TextureError(..)
    -- * Texture types
  , Flat
  , CubeMap
  , ArrayOf
  , TextureLayers(..)

    -- * Utilities
  , createImageView
  , imageCI
  , imageAllocationCI
  , stageBufferCI
  , stageAllocationCI
  , subrColor0
  ) where

import Control.Exception (Exception)
import Control.Monad.IO.Class (MonadIO(..))
import Data.Bits ((.|.))
import Data.Int (Int64)
import Data.Proxy (Proxy(..))
import Data.Text (Text)
import Data.Word (Word32)
import GHC.TypeLits (Nat, KnownNat, natVal)
import Vulkan.NamedType ((:::))
import Vulkan.Zero (zero)

import qualified Vulkan.Core10 as Vk
import qualified VulkanMemoryAllocator as VMA

import Vulkan.Setup.Context (Context)
import Vulkan.Setup.Image (AllocatedImage(..), destroyAllocatedImage)

import qualified Vulkan.Setup.Context as Context

data TextureError
  = LoadError Int64 Text
  | LayerError Word32 Word32
  | ArrayError Word32 Word32
  deriving (Eq, Ord, Show)

instance Exception TextureError

data Texture a = Texture
  { tFormat         :: Vk.Format
  , tMipLevels      :: Word32
  , tFaces          :: Word32
  , tAllocatedImage :: AllocatedImage
  }

data CubeMap
data Flat
data ArrayOf (layers :: Nat)

-- | Number of expected texture layers to load from resource.
class TextureLayers a where
  textureLayers :: Word32

instance TextureLayers CubeMap where
  textureLayers = 6

instance TextureLayers Flat where
  textureLayers = 1

instance KnownNat n => TextureLayers (ArrayOf n) where
  textureLayers = fromInteger $ natVal (Proxy @n)

destroy :: MonadIO io => Context w -> Texture a -> io ()
destroy ctx Texture{tAllocatedImage} =
  destroyAllocatedImage ctx tAllocatedImage

createImageView
  :: (MonadIO io, Context.HasVkLogical ctx)
  => ctx
  -> Vk.Image
  -> Vk.Format
  -> "mip levels" ::: Word32
  -> "array layers" ::: Word32
  -> io Vk.ImageView
createImageView ctx image format mipLevels arrayLayers =
  Vk.createImageView (Context.getVkLogical ctx) imageViewCI Nothing
  where
    imageViewCI = zero
      { Vk.image            = image
      , Vk.viewType         = viewType
      , Vk.format           = format
      , Vk.components       = zero
      , Vk.subresourceRange = subrColor0 mipLevels arrayLayers
      }
    viewType =
      if arrayLayers == 6 then
        Vk.IMAGE_VIEW_TYPE_CUBE
      else
        Vk.IMAGE_VIEW_TYPE_2D

imageCI :: Vk.Format -> Vk.Extent3D -> Word32 -> Word32 -> Vk.ImageCreateInfo '[]
imageCI format extent mipLevels arrayLayers = zero
  { Vk.flags         = flags
  , Vk.imageType     = Vk.IMAGE_TYPE_2D
  , Vk.format        = format
  , Vk.extent        = extent
  , Vk.mipLevels     = mipLevels
  , Vk.arrayLayers   = if isCube then 6 else arrayLayers
  , Vk.tiling        = Vk.IMAGE_TILING_OPTIMAL
  , Vk.initialLayout = Vk.IMAGE_LAYOUT_UNDEFINED
  , Vk.usage         = usage
  , Vk.sharingMode   = Vk.SHARING_MODE_EXCLUSIVE
  , Vk.samples       = Vk.SAMPLE_COUNT_1_BIT -- XXX: no multisampling here
  }
  where
    isCube =
      arrayLayers == 6

    usage =
      Vk.IMAGE_USAGE_SAMPLED_BIT .|.  -- Sampler
      Vk.IMAGE_USAGE_TRANSFER_DST_BIT -- Staging

    flags =
      if isCube then
        Vk.IMAGE_CREATE_CUBE_COMPATIBLE_BIT
      else
        zero

imageAllocationCI :: VMA.AllocationCreateInfo
imageAllocationCI = zero
  { VMA.usage         = VMA.MEMORY_USAGE_GPU_ONLY
  , VMA.requiredFlags = Vk.MEMORY_PROPERTY_DEVICE_LOCAL_BIT
  }

stageBufferCI :: Integral a => a -> Vk.BufferCreateInfo '[]
stageBufferCI pixelBytes = zero
  { Vk.size        = fromIntegral pixelBytes
  , Vk.usage       = Vk.BUFFER_USAGE_TRANSFER_SRC_BIT
  , Vk.sharingMode = Vk.SHARING_MODE_EXCLUSIVE
  }

stageAllocationCI :: VMA.AllocationCreateInfo
stageAllocationCI = zero
  { VMA.flags         = VMA.ALLOCATION_CREATE_MAPPED_BIT
  , VMA.usage         = VMA.MEMORY_USAGE_CPU_TO_GPU
  , VMA.requiredFlags = Vk.MEMORY_PROPERTY_HOST_VISIBLE_BIT
  }

subrColor0 :: "mip levels" ::: Word32 -> "array layers" ::: Word32 -> Vk.ImageSubresourceRange
subrColor0 levels layers = Vk.ImageSubresourceRange
  { aspectMask     = Vk.IMAGE_ASPECT_COLOR_BIT
  , baseMipLevel   = 0
  , baseArrayLayer = 0
  , levelCount     = levels
  , layerCount     = layers
  }
