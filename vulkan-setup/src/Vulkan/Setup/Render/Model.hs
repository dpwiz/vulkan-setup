module Vulkan.Setup.Render.Model where

import Control.Monad (when)
import Control.Monad.IO.Class (MonadIO(..))
import Data.Word (Word32)
import Foreign (Storable)

import qualified Data.List as List
import qualified Data.Vector as Vector
import qualified Data.Vector.Storable as Storable
import qualified Vulkan.Core10 as Vk

import Vulkan.Setup.Context (Context)

import qualified Vulkan.Setup.Buffer as Buffer

data Indexed storage pos attrs = Indexed
  { iPositions :: Buffer.Allocated storage pos
  , iAttrs     :: Buffer.Allocated storage attrs
  , iIndices   :: Buffer.Allocated storage Word32
  } deriving (Show)

data Vertex pos attrs = Vertex
  { vPosition :: pos
  , vAttrs    :: attrs
  } deriving (Show)

class HasVertexBuffers a where
  getVertexBuffers :: a -> [Vk.Buffer]
  getInstanceCount :: a -> Word32

instance HasVertexBuffers () where
  {-# INLINE getVertexBuffers #-}
  getVertexBuffers () = []

  {-# INLINE getInstanceCount #-}
  getInstanceCount () = 1

instance HasVertexBuffers (Buffer.Allocated store a) where
  {-# INLINE getVertexBuffers #-}
  getVertexBuffers Buffer.Allocated{aBuffer} = [aBuffer]

  {-# INLINE getInstanceCount #-}
  getInstanceCount = Buffer.aUsed

drawIndexed_
  :: (MonadIO io, HasVertexBuffers instances)
  => Vk.CommandBuffer
  -> Indexed storage pos attrs
  -> instances -- XXX: user should provide instances appropriate for current pipeline
  -> io ()     -- XXX: this can be indexed by pipeline type to check layout compatibility
drawIndexed_ cmd Indexed{..} instances =
  drawIndexed cmd Indexed{..} instances Nothing 0

drawIndexed
  :: (MonadIO io, HasVertexBuffers instances)
  => Vk.CommandBuffer
  -> Indexed storage pos attrs
  -> instances -- XXX: user should provide instances appropriate for current pipeline
  -> Maybe Word32
  -> Word32
  -> io ()     -- XXX: this can be indexed by pipeline type to check layout compatibility
drawIndexed cmd Indexed{..} instances instanceCount' firstInstance' =
  when (instanceCount > 0) $ liftIO do
    Vk.cmdBindVertexBuffers cmd 0 vertexBuffers bufferOffsets
    Vk.cmdBindIndexBuffer cmd (Buffer.aBuffer iIndices) 0 Vk.INDEX_TYPE_UINT32
    Vk.cmdDrawIndexed cmd (Buffer.aUsed iIndices) instanceCount firstIndex vertexOffset firstInstance
  where
    instanceCount = case instanceCount' of
      Nothing ->
        getInstanceCount instances
      Just n ->
        min n $ getInstanceCount instances

    firstIndex = 0
    vertexOffset = 0
    firstInstance = min instanceCount firstInstance'

    vertexBuffers = Vector.fromList $
      Buffer.aBuffer iPositions :
      Buffer.aBuffer iAttrs :
      getVertexBuffers instances

    bufferOffsets =
      Vector.replicate (Vector.length vertexBuffers) 0

createStaged
  :: (Storable pos, Storable attrs, MonadIO io)
  => Context w
  -> Vk.CommandPool
  -> [Vertex pos attrs]
  -> Maybe [Word32]
  -> io (Indexed 'Buffer.Staged pos attrs)
createStaged context pool vertices mindices = do
  positions <- Buffer.createStaged context pool Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT 0 pv
  attrs     <- Buffer.createStaged context pool Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT 0 av
  indices   <- Buffer.createStaged context pool Vk.BUFFER_USAGE_INDEX_BUFFER_BIT  0 iv

  pure Indexed
    { iPositions = positions
    , iAttrs     = attrs
    , iIndices   = indices
    }
  where
    pv = Storable.fromList ps
    av = Storable.fromList as

    iv = case mindices of
      Just is ->
        Storable.fromList is
      Nothing ->
        -- TODO: add vertex deduplication
        Storable.generate (Storable.length pv) fromIntegral

    (ps, as) = List.unzip do
      Vertex{..} <- vertices
      pure (vPosition, vAttrs)

createCoherent
  :: (Storable pos, Storable attrs, MonadIO io)
  => Context w
  -> Int
  -> io (Indexed 'Buffer.Coherent pos attrs)
createCoherent ctx initialSize = Indexed
  <$> Buffer.createCoherent ctx Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT initialSize mempty
  <*> Buffer.createCoherent ctx Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT initialSize mempty
  <*> Buffer.createCoherent ctx Vk.BUFFER_USAGE_INDEX_BUFFER_BIT  initialSize mempty

destroyIndexed
  :: MonadIO io
  => Context w
  -> Indexed storage pos attrs
  -> io ()
destroyIndexed ctx Indexed{..} = do
  Buffer.destroyAll ctx [iPositions]
  Buffer.destroyAll ctx [iAttrs]
  Buffer.destroyAll ctx [iIndices]

updateCoherent
  :: (Storable pos, Storable attrs, MonadIO io)
  => Context w
  -> [Vertex pos attrs]
  -> Indexed 'Buffer.Coherent pos attrs
  -> io (Indexed 'Buffer.Coherent pos attrs)
updateCoherent ctx vertices old = do
  Indexed{..} <- pick
  Indexed
    <$> Buffer.updateCoherent pv iPositions
    <*> Buffer.updateCoherent av iAttrs
    <*> Buffer.updateCoherent iv iIndices
  where
    pick =
      if oldSize > newSize then
        pure old
      else do
        destroyIndexed ctx old
        createCoherent ctx (max newSize $ oldSize * 2)

    oldSize = Buffer.aCapacity $ iIndices old
    newSize = Storable.length pv

    pv = Storable.fromList ps
    av = Storable.fromList as

    iv = Storable.generate newSize fromIntegral

    (ps, as) = List.unzip do
      Vertex{..} <- vertices
      pure (vPosition, vAttrs)
