module Vulkan.Setup.Logical where

import Control.Monad.IO.Class (MonadIO(..))
import Data.Vector (Vector)
import Data.Text (Text)
import Vulkan.CStruct.Extends (SomeStruct(..))
import Vulkan.Zero (zero)

import qualified Data.List as List
import qualified Data.Vector as Vector
import qualified Data.Text.Encoding as Text
import qualified Vulkan.Core10 as Vk
import qualified Vulkan.Extensions.VK_KHR_swapchain as Khr

import qualified Vulkan.Setup.Physical as Physical

data Device = Device
  { logical       :: Vk.Device
  , graphicsQueue :: Vk.Queue
  , presentQueue  :: Vk.Queue
  } deriving (Show)

getDevice
  :: MonadIO io
  => Physical.Device
  -> Khr.SurfaceKHR
  -> Vector Text
  -> io Device
getDevice pd _surface layers = do
  device <- Vk.createDevice physical ci Nothing

  graphicsQueue <- Vk.getDeviceQueue device graphicsQueueIx zero
  presentQueue <- Vk.getDeviceQueue device presentQueueIx zero

  pure Device
    { logical = device
    , graphicsQueue
    , presentQueue
    }
  where
    Physical.Device
      { physical
      , msaa
      , samplerAnisotropy
      , graphicsQueueIx
      , presentQueueIx
      } = pd

    ci = zero
      { Vk.queueCreateInfos      = Vector.fromList queues
      , Vk.enabledLayerNames     = Vector.map Text.encodeUtf8 layers
      , Vk.enabledExtensionNames = Vector.fromList extensions
      , Vk.enabledFeatures       = Just features
      }

    queues = do
      ix <- List.nub [graphicsQueueIx, presentQueueIx]
      pure $ SomeStruct zero
        { Vk.queueFamilyIndex = ix
        , Vk.queuePriorities  = Vector.singleton 1
        }

    extensions =
      [ Khr.KHR_SWAPCHAIN_EXTENSION_NAME
      ]

    features = zero
      { Vk.sampleRateShading = msaa /= Vk.SAMPLE_COUNT_1_BIT
      , Vk.samplerAnisotropy = samplerAnisotropy > 1.0
      }
