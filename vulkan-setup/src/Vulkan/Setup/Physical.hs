module Vulkan.Setup.Physical where

import Control.Monad (guard, when, join)
import Control.Monad.IO.Class (MonadIO(..))
import Data.Bits (Bits, zeroBits, (.&.))
import Data.Maybe (listToMaybe)
import Data.Traversable (for)
import Data.Vector (Vector)
import Debug.Trace (traceM)
import GHC.Word (Word32)

import qualified Data.Vector as Vector
import qualified Vulkan.Core10 as Vk
import qualified Vulkan.Core11 as Vk11
import qualified Vulkan.Extensions.VK_KHR_surface as Khr

data Device = Device
  { physical            :: Vk.PhysicalDevice
  , properties          :: Vk.PhysicalDeviceProperties
  , features            :: Vk.PhysicalDeviceFeatures
  , memoryProperties    :: Vk.PhysicalDeviceMemoryProperties

  , graphicsQueueIx     :: Word32
  , presentQueueIx      :: Word32
  , presentMode         :: Khr.PresentModeKHR
  , depthFormat         :: Vk.Format
  , surfaceFormat       :: Khr.SurfaceFormatKHR

  , msaa                :: Vk.SampleCountFlagBits
  , samplerAnisotropy   :: Float
  } deriving (Show)

getDevices :: MonadIO m => Vk.Instance -> Khr.SurfaceKHR -> m (Vector Device)
getDevices instance_ surface = do
  (_result, devs) <- Vk.enumeratePhysicalDevices instance_

  when (Vector.null devs) $
    traceM "Warning: no physical devices"

  candidates <- for devs \device -> do
    props <- Vk.getPhysicalDeviceProperties device
    feats <- Vk.getPhysicalDeviceFeatures device
    mem   <- Vk.getPhysicalDeviceMemoryProperties device

    graphicsQueueIxs <- getGraphicsQueueIndices device
    presentQueueIxs  <- getPresentQueueIndices device surface
    presentModes     <- getPresentMode device surface preferFifo
    depthFormats     <- getDepthFormats device preferStenciledDepth

    let
      matching = (,,,)
        <$> listToMaybe graphicsQueueIxs
        <*> listToMaybe presentQueueIxs
        <*> listToMaybe presentModes
        <*> listToMaybe depthFormats
    for matching \(graphicsQueueIx, presentQueueIx, presentMode, depthFormat) -> do
      surfaceFormat <- getSurfaceFormat device surface preferSrgb

      pure $ Just Device
        { physical            = device
        , properties          = props
        , features            = feats
        , memoryProperties    = mem

        , graphicsQueueIx     = graphicsQueueIx
        , presentQueueIx      = presentQueueIx
        , presentMode         = presentMode
        , depthFormat         = depthFormat
        , surfaceFormat       = surfaceFormat

        , msaa                = msaaSamples props
        , samplerAnisotropy   = Vk.maxSamplerAnisotropy (Vk.limits props)
        }

  pure $ Vector.mapMaybe join candidates

getGraphicsQueueIndices
  :: MonadIO io
  => Vk.PhysicalDevice -> io [Word32]
getGraphicsQueueIndices dev = do
  queueFamilyProperties <- Vk.getPhysicalDeviceQueueFamilyProperties dev
  pure do
    (ix, q) <- zip [0..] $ Vector.toList queueFamilyProperties
    guard $
      (Vk.QUEUE_GRAPHICS_BIT .&&. Vk.queueFlags q) &&
      (Vk.queueCount q > 0)
    pure ix

getPresentQueueIndices
  :: MonadIO io
  => Vk.PhysicalDevice
  -> Khr.SurfaceKHR
  -> io [Word32]
getPresentQueueIndices dev surface = fmap concat do
  queues <- Vk11.getPhysicalDeviceQueueFamilyProperties2 @'[] dev
  for (zip [0..] $ Vector.toList queues) \(ix, _q) -> do
    support <- Khr.getPhysicalDeviceSurfaceSupportKHR dev ix surface
    pure [ix | support]

preferDiscrete :: Vector Device -> Device
preferDiscrete devices =
  if not (Vector.null discrete) then
    Vector.head discrete
  else
    Vector.head devices
  where
    discrete = Vector.filter isDiscreteGPU devices

    isDiscreteGPU Device{properties} =
      Vk.deviceType properties == Vk.PHYSICAL_DEVICE_TYPE_DISCRETE_GPU

getPresentMode
  :: MonadIO io
  => Vk.PhysicalDevice
  -> Khr.SurfaceKHR
  -> [Khr.PresentModeKHR]
  -> io [Khr.PresentModeKHR]
getPresentMode dev surface desiredPresentModes = do
  (_res, presentModes) <- Khr.getPhysicalDeviceSurfacePresentModesKHR dev surface
  pure do
    m <- desiredPresentModes
    guard $ Vector.elem m presentModes
    pure m

preferFifo :: [Khr.PresentModeKHR]
preferFifo =
  [ Khr.PRESENT_MODE_FIFO_KHR
  , Khr.PRESENT_MODE_MAILBOX_KHR
  , Khr.PRESENT_MODE_IMMEDIATE_KHR
  ]

getSurfaceFormat
  :: MonadIO io
  => Vk.PhysicalDevice
  -> Khr.SurfaceKHR
  -> Khr.SurfaceFormatKHR
  -> io Khr.SurfaceFormatKHR
getSurfaceFormat device surface desiredFormat = do
  (_res, formats) <- Khr.getPhysicalDeviceSurfaceFormatsKHR device surface
  pure case Vector.toList formats of
    [] ->
      desiredFormat
    [Khr.SurfaceFormatKHR Vk.FORMAT_UNDEFINED _colorSpace] ->
      desiredFormat
    candidates | any cond candidates ->
      desiredFormat
    whatever : _rest ->
      whatever
  where
    cond f =
      Khr.format f == Khr.format desiredFormat &&
      Khr.colorSpace f == Khr.colorSpace desiredFormat

preferSrgb :: Khr.SurfaceFormatKHR
preferSrgb =
  Khr.SurfaceFormatKHR
    Vk.FORMAT_B8G8R8_SRGB
    Khr.COLOR_SPACE_SRGB_NONLINEAR_KHR

getDepthFormats
  :: MonadIO io
  => Vk.PhysicalDevice
  -> [Vk.Format]
  -> io [Vk.Format]
getDepthFormats device desiredDepthFormats = do
  properties <- traverse (Vk.getPhysicalDeviceFormatProperties device) desiredDepthFormats
  pure do
    (format, props) <- zip desiredDepthFormats properties
    guard $
      Vk.optimalTilingFeatures props .&&. Vk.FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT
    pure format

preferStenciledDepth :: [Vk.Format]
preferStenciledDepth =
  [ Vk.FORMAT_D32_SFLOAT_S8_UINT
  , Vk.FORMAT_D24_UNORM_S8_UINT
  , Vk.FORMAT_D32_SFLOAT
  ]

msaaSamples :: Vk.PhysicalDeviceProperties -> Vk.SampleCountFlagBits
msaaSamples Vk.PhysicalDeviceProperties{limits} =
  case samplesAvailable of
    [] ->
      Vk.SAMPLE_COUNT_1_BIT
    best : _rest ->
      best
  where
    counts =
      Vk.framebufferColorSampleCounts limits .&.
      Vk.framebufferDepthSampleCounts limits

    samplesAvailable = do
      countBit <- msaaCandidates
      guard $ (counts .&. countBit) /= zeroBits
      pure countBit

msaaCandidates :: [Vk.SampleCountFlagBits]
msaaCandidates =
  [ Vk.SAMPLE_COUNT_16_BIT
  , Vk.SAMPLE_COUNT_8_BIT
  , Vk.SAMPLE_COUNT_4_BIT
  , Vk.SAMPLE_COUNT_2_BIT
  ]

(.&&.) :: Bits a => a -> a -> Bool
a .&&. b = a .&. b /= zeroBits
