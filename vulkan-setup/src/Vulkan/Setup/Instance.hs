module Vulkan.Setup.Instance where

import Control.Monad.IO.Class (MonadIO)
import Data.ByteString (ByteString)
import Data.Text (Text)
import Data.Text.Encoding (encodeUtf8)
import Data.Word (Word32)
import Vulkan.CStruct.Extends (Extendss, PokeChain)
import Vulkan.NamedType ((:::))
import Vulkan.Version (pattern MAKE_VERSION)
import Vulkan.Zero (zero)

import qualified Data.Vector as Vector
import qualified Vulkan.Core10 as Vk

type Mods mods = Vk.InstanceCreateInfo '[] -> Vk.InstanceCreateInfo mods

version11 :: Word32
version11 = MAKE_VERSION 1 1 0

create
  :: (MonadIO io, PokeChain mods, Extendss Vk.InstanceCreateInfo mods)
  => "version" ::: Word32
  -> Text
  -> "layers" ::: [Text]
  -> "extensions" ::: [ByteString]
  -> "instance mods" ::: Mods mods
  -> io Vk.Instance
create version title layers extensions withCI =
  Vk.createInstance (withCI instanceCI) Nothing
  where
    instanceCI = zero
      { Vk.enabledLayerNames =
          Vector.fromList $ map encodeUtf8 layers
      , Vk.enabledExtensionNames =
          Vector.fromList extensions
      , Vk.applicationInfo = Just zero
          { Vk.applicationName = Just $ encodeUtf8 title
          , Vk.apiVersion      = version
          }
      }

validationLayers :: [Text]
validationLayers =
  [ "VK_LAYER_KHRONOS_validation"
  ]
