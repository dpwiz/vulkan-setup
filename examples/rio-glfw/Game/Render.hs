module Game.Render where

import App.Prelude

import Vulkan.Exception (vulkanExceptionResult)
import Vulkan.Setup.Exceptions (throwVkResult)

import qualified RIO.Vector as Vector
import qualified Vulkan.Core10 as Vk
import qualified Vulkan.Extensions.VK_KHR_swapchain as Khr
import qualified Vulkan.Setup.Logical as Logical
import qualified Vulkan.Setup.Render.Forward as Forward
import qualified Vulkan.Setup.Render.Model as Model
import qualified Vulkan.Setup.Render.Pipeline as Pipeline
import qualified Vulkan.Setup.Swapchain as Swapchain
import qualified Vulkan.Setup.Context as Context

import Game.Setup.Env (Env(..))
import Render.Types (ForSwapchain(..), Inflight(..), Pipelines(..))

import qualified Render.Pipeline.Triangle as Triangle

drawFrame :: Env -> RIO App () -> RIO App ()
drawFrame = withNextImage frameRender

frameRender :: Env -> Vk.CommandBuffer -> ForSwapchain -> RIO App ()
frameRender Env{..} cmd ForSwapchain{..} = do
  let Pipelines{..} = fsPipelines

  Pipeline.bind cmd triangle
  Triangle.draw_ cmd

  Vk.cmdBindDescriptorSets
    cmd
    Vk.PIPELINE_BIND_POINT_GRAPHICS
    (Pipeline.pLayout solid)
    0
    (Vector.singleton fsSet0)
    mempty

  Pipeline.bind cmd solid
  Model.drawIndexed_ cmd envModel envCenter

  positions <- readMVar envPositions
  Pipeline.bind cmd line
  Model.drawIndexed_ cmd positions envCenter

withNextImage
  :: (Env -> Vk.CommandBuffer -> ForSwapchain -> RIO App ())
  -> Env
  -> RIO App ()
  -> RIO App ()
withNextImage render env@Env{..} setRestart = do
  vkLogical <- Context.askVkLogical

  Inflight{..} <- modifyMVar envInflights \(cur, next) ->
    pure
      ( (next, cur)
      , cur
      )

  Vk.waitForFences vkLogical (pure _iReady) True maxBound >>= \case
    Vk.SUCCESS ->
      pure ()
    Vk.TIMEOUT ->
      logWarn "waitForFences: timed out"
    err ->
      throwVkResult err

  releaseRef <- newIORef Nothing

  fs@ForSwapchain{..} <- atomically $ readTMVar envSwapchain
  let
    vkSwapchain = Swapchain.swapchain fsSwapchain

    nextImage =
      Khr.acquireNextImageKHR
      vkLogical
      vkSwapchain
      maxBound -- XXX: timeout in nanoseconds
      _iImageAvailable
      zero

  try nextImage >>= \case
    Right (Vk.SUCCESS, imageIndex) -> do
      Logical.Device{graphicsQueue, presentQueue} <- asks $ Context.logical . appContext

      let
        cbAI = zero
          { Vk.commandPool        = _iCommandPool
          , Vk.level              = Vk.COMMAND_BUFFER_LEVEL_PRIMARY
          , Vk.commandBufferCount = 1
          }
      Vk.resetCommandPool vkLogical _iCommandPool Vk.COMMAND_POOL_RESET_RELEASE_RESOURCES_BIT
      bufs <- Vk.allocateCommandBuffers vkLogical cbAI
      atomicWriteIORef releaseRef . Just $ -- XXX: can be a monoid
        Vk.freeCommandBuffers vkLogical _iCommandPool bufs

      commandBuffer <- case Vector.toList bufs of
        [cb] -> do
          let
            commandBufferBI = zero
              { Vk.flags = Vk.COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT
              } :: Vk.CommandBufferBeginInfo '[]
          Vk.useCommandBuffer cb commandBufferBI do
            Forward.usePass fsRender imageIndex cb $
              render env cb fs
          pure cb
        _ ->
          error "assert: withCommandBuffers allocates a single CB"

      let
        submitInfo = Vector.singleton $ SomeStruct zero
          { Vk.waitSemaphores   = Vector.singleton _iImageAvailable
          , Vk.waitDstStageMask = Vector.singleton Vk.PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT
          , Vk.commandBuffers   = Vector.singleton (Vk.commandBufferHandle commandBuffer)
          , Vk.signalSemaphores = Vector.singleton _iRenderFinished
          }
      Vk.resetFences vkLogical $ Vector.singleton _iSubmitted
      Vk.queueSubmit graphicsQueue submitInfo _iSubmitted

      let
        presentInfo = zero
          { Khr.waitSemaphores = Vector.singleton _iRenderFinished
          , Khr.swapchains     = Vector.singleton vkSwapchain
          , Khr.imageIndices   = Vector.singleton imageIndex
          }

      try (Khr.queuePresentKHR presentQueue presentInfo) >>= \case
        Right Vk.SUCCESS ->
          pure ()
        Right err ->
          throwVkResult err
        Left err ->
          case vulkanExceptionResult err of
            Vk.ERROR_OUT_OF_DATE_KHR ->
              setRestart
            _ -> do
              logError $ "queuePresentKHR: " <> displayShow err
              throwM err

    Right (Vk.SUBOPTIMAL_KHR, _) ->
      setRestart
    Right (err, _) ->
      throwVkResult err
    Left err ->
      case vulkanExceptionResult err of
        Vk.ERROR_OUT_OF_DATE_KHR ->
          setRestart
        _ -> do
          logError $ displayShow err
          throwM err

  void $ Vk.waitForFences vkLogical (pure _iSubmitted) True maxBound
  -- traverse_ release transient
  readIORef releaseRef >>= fromMaybe mempty
