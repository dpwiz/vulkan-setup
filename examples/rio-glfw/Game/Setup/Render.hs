module Game.Setup.Render where

import App.Prelude

import Vulkan.Setup.Swapchain (Swapchain)

import qualified Vulkan.Core10 as Vk

import Game.Setup.Env (Env(..))

import qualified Render.Swapchain as Swapchain

-- TODO: move to Render.Setup
afterSwapchainCreate :: Env -> Swapchain Vk.ImageView -> RIO App ()
afterSwapchainCreate env@Env{..} swapchain = do
  logInfo "Swapchain created"

  new <- Swapchain.createForSwapchain env swapchain

  discard <- atomically do
    discard <- tryTakeTMVar envSwapchain
    putTMVar envSwapchain new
    pure discard
  case discard of
    Nothing ->
      pure ()
    Just old ->
      Swapchain.destroyForSwapchain old

  atomicModifyIORef' envMainLoops \old ->
    ( case old of
        -- [] -> [0]
        [0] -> [0]
        _ -> 0 : old
    , ()
    )

beforeSwapchainDestroy :: Env -> Swapchain Vk.ImageView -> RIO App ()
beforeSwapchainDestroy Env{envSwapchain} _swapchain = do
  -- TODO: destroy swapchain-derived resources
  logInfo "Destroying swapchain"

  atomically (tryTakeTMVar envSwapchain) >>= \case
    Nothing ->
      logWarn "No swapchain resources were created before swapchain got destroyed"
    Just old ->
      Swapchain.destroyForSwapchain old
