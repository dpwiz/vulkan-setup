module Game.Events where

import App.Prelude

import Game.Setup.Env (Env(..))
import Game.Render (drawFrame)

import qualified Game.Events.Time as Time

afterEvents
  :: Env
  -> (Bool -> RIO App ())
  -> RIO App ()
afterEvents env@Env{..} setQuit = do
  Time.run env
  drawFrame env (setQuit False)
  atomicModifyIORef' envMainLoops \case
    [] ->
      error "assert: afterSwapchainCreate has to initialize this"
    n : rest ->
      ( n + 1 : rest
      , ()
      )
