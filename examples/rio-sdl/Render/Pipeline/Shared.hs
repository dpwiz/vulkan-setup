module Render.Pipeline.Shared where

import Prelude

import qualified Vulkan.Core10 as Vk

set0 :: [Vk.DescriptorSetLayoutBinding]
set0 =
  [ set0bind0
  ]

set0bind0 :: Vk.DescriptorSetLayoutBinding
set0bind0 = Vk.DescriptorSetLayoutBinding
  { binding           = 0
  , descriptorType    = Vk.DESCRIPTOR_TYPE_UNIFORM_BUFFER
  , descriptorCount   = 1
  , stageFlags        = Vk.SHADER_STAGE_ALL
  , immutableSamplers = mempty
  }

vertexPos :: (Vk.VertexInputRate, [Vk.Format])
vertexPos =
  ( Vk.VERTEX_INPUT_RATE_VERTEX
  , [Vk.FORMAT_R32G32B32_SFLOAT]
  )
