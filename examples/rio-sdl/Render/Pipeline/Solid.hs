module Render.Pipeline.Solid where

import App.Prelude

import Vulkan.Setup.Render.Pipeline (Pipeline)
import Vulkan.Setup.Render.Model (Vertex(..))
import Vulkan.Utils.ShaderQQ (vert, frag)

import qualified Linear
import qualified Data.Vector.Storable as Storable
import qualified Vulkan.Core10 as Vk
import qualified Vulkan.Setup.Buffer as Buffer
import qualified Vulkan.Setup.Render.Model as Model
import qualified Vulkan.Setup.Render.Pipeline as Pipeline

import qualified Render.Pipeline.Shared as Shared

type Coherent = Model.Indexed 'Buffer.Coherent Pos Attrs
type Staged = Model.Indexed 'Buffer.Staged Pos Attrs

type Pos = Linear.V3 Float
type Attrs = Linear.V4 Float

type CoherentInstances = Buffer.Allocated 'Buffer.Coherent Instance

newtype Instance = Instance
  { iTransform :: Linear.M44 Float
  }
  deriving (Storable)

create :: Vk.Extent2D -> Vk.RenderPass -> RIO App Pipeline
create extent renderPass = do
  context <- asks appContext

  Pipeline.create context extent renderPass zero
    { Pipeline.cVertexCode   = Just vertCode
    , Pipeline.cFragmentCode = Just fragCode
    , Pipeline.cDescLayouts  = [Shared.set0]
    , Pipeline.cVertexInput  = vertexInput
    }
  where
    vertexInput = Pipeline.vertexInput
      [ Shared.vertexPos
      , (Vk.VERTEX_INPUT_RATE_VERTEX,   vertexAttrs)
      , (Vk.VERTEX_INPUT_RATE_INSTANCE, instanceAttrs)
      ]

    vertexAttrs =
      [ Vk.FORMAT_R32G32B32A32_SFLOAT
      ]

    {-
      XXX: mat4s can not be passed as-is
      and need to be split to their vec4s
      and reassembled in vertex shader.
    -}
    instanceAttrs =
      [ Vk.FORMAT_R32G32B32A32_SFLOAT
      , Vk.FORMAT_R32G32B32A32_SFLOAT
      , Vk.FORMAT_R32G32B32A32_SFLOAT
      , Vk.FORMAT_R32G32B32A32_SFLOAT
      ]

vertCode :: ByteString
vertCode =
  [vert|
    #version 450
    #extension GL_ARB_separate_shader_objects : enable

    layout(set=0, binding=0) uniform Camera {
      mat4 VP;
    } camera;

    layout(location = 0) in vec3 vPosition;
    layout(location = 1) in vec4 vColor;

    // XXX: due to some instance attribute silliness
    layout(location = 2) in vec4 iTransform0;
    layout(location = 3) in vec4 iTransform1;
    layout(location = 4) in vec4 iTransform2;
    layout(location = 5) in vec4 iTransform3;

    layout(location = 0) out vec4 fragColor;

    mat4 iTransform = mat4(
      iTransform0,
      iTransform1,
      iTransform2,
      iTransform3
    );

    void main() {
      gl_Position = camera.VP * iTransform * vec4(vPosition, 1.0);
      fragColor = vColor;
    }
  |]

fragCode :: ByteString
fragCode =
  [frag|
    #version 450
    #extension GL_ARB_separate_shader_objects : enable

    layout(location = 0) in vec4 fragColor;

    layout(location = 0) out vec4 outColor;

    void main() {
      outColor = fragColor;
    }
  |]

squareVertices :: [Model.Vertex Pos Attrs]
squareVertices =
  [ Vertex
      { vPosition = Linear.V3 (-0.5) (-0.5) 0
      , vAttrs    = Linear.V4 0.5 0.5 0 1
      }
  , Vertex
      { vPosition = Linear.V3 (0.5) (-0.5) 0
      , vAttrs    = Linear.V4 0 0.5 0.5 1
      }
  , Vertex
      { vPosition = Linear.V3 (-0.5) (0.5) 0
      , vAttrs    = Linear.V4 0.5 0 0.5 1
      }
  , Vertex
      { vPosition = Linear.V3 (0.5) (0.5) 0
      , vAttrs    = Linear.V4 0.5 0.5 0.5 1
      }
  ]

squareIndices :: [Word32]
squareIndices =
  [ 0, 1, 2
  , 3, 2, 1
  ]

createInstance
  :: Pos
  -> "turn"  ::: Float
  -> "scale" ::: Float
  -> RIO App (Buffer.Allocated 'Buffer.Coherent Instance)
createInstance (Linear.V3 tx ty tz) _turn sc =
  createInstances . Storable.singleton $ Instance transform
  where
    transform = Linear.V4
      (Linear.V4 sc  0  0 0)
      (Linear.V4  0 sc  0 0)
      (Linear.V4  0  0 sc 0)
      (Linear.V4 tx ty tz 1)

createInstances
  :: Storable.Vector Instance
  -> RIO App (Buffer.Allocated 'Buffer.Coherent Instance)
createInstances is = do
  ctx <- asks appContext
  Buffer.createCoherent ctx Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT 0 is
