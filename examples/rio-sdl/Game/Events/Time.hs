module Game.Events.Time where

import App.Prelude

import Vulkan.Setup.Render.Model (Vertex(..))

import qualified Linear
import qualified SDL
import qualified Vulkan.Setup.Render.Model as Model

import Game.Setup.Env (Env(..))

run :: Env -> RIO App ()
run Env{..} = do
  ms <- SDL.ticks
  ctx <- asks appContext

  let
    t = fract (fromIntegral ms / 30_000) * tau
    n = 1_000 :: Int
    stuff = take (max 1 . round $ 500 - 500 * cos t) do
      i <- [0..n]

      let
        t' = t + fromIntegral i * tau / fromIntegral n

      let
        x = 1.5 * (0.5 * cos (2 * t') + 0.5 * sin (3 * t') + 0.5 * sin (5 * t'))
        y = 1.5 * (0.5 * sin (2 * t') + 0.5 * cos (3 * t') + 0.5 * cos (5 * t'))
        z = 0

      let
        r = 0.5 + 0.5 * cos t'
        g = 0.5 + 0.5 * sin t'
        b = 0.5 + 0.5 * cos t' * sin t'

      pure Vertex
        { vPosition = fmap (* 0.5) $ Linear.V3 x y z
        , vAttrs    = Linear.V4 r g b 1
        }

  modifyMVarMasked_ envPositions $
    Model.updateCoherent ctx stuff
