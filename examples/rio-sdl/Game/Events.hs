module Game.Events where

import App.Prelude

import qualified SDL

import Game.Setup.Env (Env(..))
import Game.Render (drawFrame)

import qualified Game.Events.Time as Time

handleEvent
  :: Env
  -> (Bool -> RIO App ())
  -> SDL.Event
  -> RIO App ()
handleEvent _env setQuit SDL.Event{eventPayload} =
  case eventPayload of
    SDL.QuitEvent ->
      setQuit True

    SDL.KeyboardEvent SDL.KeyboardEventData{..} ->
      case SDL.keysymKeycode keyboardEventKeysym of
        SDL.KeycodeEscape ->
          setQuit True
        _anyKey ->
          pure ()

    SDL.WindowResizedEvent{} ->
      setQuit False

    _rest ->
      pure ()

afterEvents
  :: Env
  -> (Bool -> RIO App ())
  -> RIO App ()
afterEvents env@Env{..} setQuit = do
  Time.run env
  drawFrame env (setQuit False)
  atomicModifyIORef' envMainLoops \case
    [] ->
      error "assert: afterSwapchainCreate has to initialize this"
    n : rest ->
      ( n + 1 : rest
      , ()
      )
