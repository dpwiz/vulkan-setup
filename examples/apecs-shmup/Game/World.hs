{-# LANGUAGE TemplateHaskell #-}

module Game.World where

import Apecs
import Apecs.TH (makeMapComponents)
import Control.Monad.IO.Class (MonadIO)
import Linear (V2)

newtype Position = Position (V2 Float)
  deriving Show

newtype Velocity = Velocity (V2 Float)
  deriving Show

data Target = Target
  deriving Show

data Bullet = Bullet
  deriving Show

data Particle = Particle Float
  deriving Show

data Player = Player
  deriving Show

instance Component Player where
  type Storage Player = Unique Player

newtype Score = Score Int
  deriving (Show, Num)

instance Semigroup Score where
  (<>) = (+)

instance Monoid Score where
  mempty = 0

instance Component Score where
  type Storage Score = Global Score

newtype Time = Time Float
  deriving (Show, Num)

instance Semigroup Time where
  (<>) = (+)

instance Monoid Time where
  mempty = 0

instance Component Time where
  type Storage Time = Global Time

makeMapComponents
  [ ''Position
  , ''Velocity
  , ''Target
  , ''Bullet
  , ''Particle
  ]

makeWorld "World"
  [ ''Position
  , ''Velocity
  , ''Player
  , ''Target
  , ''Bullet
  , ''Score
  , ''Time
  , ''Particle
  ]

type Kinetic = (Position, Velocity)

type SystemW a = System World a

{-# INLINE runWorld #-}
runWorld :: MonadIO m => World -> SystemT World IO a -> m a
runWorld world = liftIO . runWith world
