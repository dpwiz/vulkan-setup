module App.Prelude
  ( module RE

  , (.&&.)
  , sizeOfFloats
  , tau
  , fract
  , clamp
  ) where

import RIO as RE

import Data.Bits as RE (Bits, zeroBits, (.|.), (.&.))
import Data.List as RE (sort, sortOn)
import Foreign.C.Types as RE (CInt)
import Foreign.Ptr as RE (castPtr)
import Foreign.Storable as RE (Storable(..))
import GHC.Float as RE (double2Float, float2Double)
import Vulkan.CStruct.Extends as RE (SomeStruct(..))
import Vulkan.NamedType as RE ((:::))
import Vulkan.Zero as RE (Zero(..))

import App.Types as RE

(.&&.) :: Bits a => a -> a -> Bool
a .&&. b = a .&. b /= zeroBits

sizeOfFloats :: Integral a => Int -> a
sizeOfFloats num = fromIntegral $ sizeOf @Float 0 * num

tau :: Floating a => a
tau = 2 * pi

fract :: Float -> Float
fract f = f - fromIntegral @Int (truncate f)

{-# INLINE clamp #-}
clamp :: Ord a => a -> a -> a -> a
clamp f t = max f . min t
