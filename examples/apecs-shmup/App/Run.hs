module App.Run (run) where

import App.Prelude

import qualified SDL hiding (showWindow)
import qualified Vulkan.Core10 as Vk
import qualified Vulkan.Setup.Logical as Logical
import qualified Vulkan.Setup.Window.SDL as Window
import qualified Vulkan.Setup.Swapchain as Swapchain
import qualified Vulkan.Setup.Context as Context

import Game.Setup.Env (Env, withEnv)
import Game.Setup.Render (afterSwapchainCreate, beforeSwapchainDestroy)
import Game.Events (handleEvent, afterEvents)

run :: RIO App ()
run = withEnv \env -> do
  asks appContext >>= Window.showWindow

  contextLoop env

contextLoop :: Env -> RIO App ()
contextLoop env = do
  reallyQuit <- bracket (create env) (destroy env) \_swapchain ->
    mainLoop env
  if reallyQuit then
    pure ()
  else
    contextLoop env
  where
    create _env = do
      ctx <- asks appContext
      swapchain <- Swapchain.create Nothing ctx
      afterSwapchainCreate env swapchain
      pure swapchain

    destroy _env swapchain = do
      ctx <- asks appContext
      -- TODO: signal to stop piling up more rendering tasks
      Vk.deviceWaitIdle (Logical.logical $ Context.logical ctx)
      beforeSwapchainDestroy env swapchain
      Swapchain.destroy swapchain ctx

mainLoop :: Env -> RIO App Bool
mainLoop env = do
  quitRef <- newIORef Nothing
  let setQuit = writeIORef quitRef . Just

  SDL.pollEvents >>= mapM_ (handleEvent env setQuit)
  afterEvents env setQuit

  quit <- readIORef quitRef
  case quit of
    Nothing ->
      mainLoop env
    Just flag ->
      pure flag
