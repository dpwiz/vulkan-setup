module Render.Pipeline.Line where

import App.Prelude

import Vulkan.Setup.Render.Pipeline (Pipeline)
import Vulkan.Utils.ShaderQQ (vert, frag)

import qualified Linear
import qualified Vulkan.Core10 as Vk
import qualified Vulkan.Setup.Buffer as Buffer
import qualified Vulkan.Setup.Render.Pipeline as Pipeline
import qualified Vulkan.Setup.Render.Model as Model

import qualified Render.Pipeline.Shared as Shared

type Coherent = Model.Indexed 'Buffer.Coherent Pos Attrs
type Staged = Model.Indexed 'Buffer.Staged Pos Attrs

type Pos = Linear.V3 Float
type Attrs = Linear.V4 Float

create :: Vk.Extent2D -> Vk.RenderPass -> RIO App Pipeline
create extent renderPass = do
  context <- asks appContext
  Pipeline.create context extent renderPass zero
    { Pipeline.cVertexCode   = Just vertCode
    , Pipeline.cFragmentCode = Just fragCode
    , Pipeline.cDescLayouts  = [Shared.set0]
    , Pipeline.cVertexInput  = vertexInput
    , Pipeline.cTopology     = Vk.PRIMITIVE_TOPOLOGY_LINE_LIST
    }
  where
    vertexInput = Pipeline.vertexInput
      [ Shared.vertexPos
      , (Vk.VERTEX_INPUT_RATE_VERTEX, vertexAttrs)
      ]

    vertexAttrs =
      [ Vk.FORMAT_R32G32B32A32_SFLOAT
      ]

vertCode :: ByteString
vertCode =
  [vert|
    #version 450
    #extension GL_ARB_separate_shader_objects : enable

    // XXX: shared with Solid shader so they can be in the same space
    layout(set=0, binding=0) uniform Camera {
      mat4 VP;
    } camera;

    layout(location = 0) in vec3 vPosition;
    layout(location = 1) in vec4 vColor;

    layout(location = 0) out vec4 fragColor;

    void main() {
      gl_Position = camera.VP * vec4(vPosition, 1.0);
      fragColor = vColor;
    }
  |]

fragCode :: ByteString
fragCode =
  [frag|
    #version 450
    #extension GL_ARB_separate_shader_objects : enable

    layout(location = 0) in vec4 fragColor;

    layout(location = 0) out vec4 outColor;

    void main() {
      outColor = fragColor;
    }
  |]
