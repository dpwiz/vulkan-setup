module Render.Swapchain where

import App.Prelude

import Vulkan.Setup.Swapchain (Swapchain)

import qualified Data.Vector as Vector
import qualified Data.Vector.Storable as Storable
import qualified Linear
import qualified Vulkan.Core10 as Vk
import qualified Vulkan.Setup.Buffer as Buffer
import qualified Vulkan.Setup.Render.Forward as Forward
import qualified Vulkan.Setup.Render.Pipeline as Pipeline
import qualified Vulkan.Setup.Swapchain as Swapchain
import qualified Vulkan.Setup.Context as Context

import qualified Render.Pipeline.Line as Line
import qualified Render.Pipeline.Solid as Solid
import qualified Render.Types as Render

import Game.Setup.Env (Env(..))

-- * Swapchain image resources

createForSwapchain :: Env -> Swapchain Vk.ImageView -> RIO App Render.ForSwapchain
createForSwapchain env swapchain = do
  context <- asks appContext
  let vkLogical = Context.getVkLogical context

  forward <- Forward.createMsaa context swapchain
  let forwardPass = Forward.fmRenderPass forward

  pipelines <- Render.Pipelines
    <$> Solid.create    extent forwardPass
    <*> Line.create     extent forwardPass

  -- XXX: use this pipeline as a refernce for a shared layout
  let set0layout = Vector.take 1 $ Pipeline.pDescLayouts (Render.solid pipelines)

  descPool <- Vk.createDescriptorPool vkLogical dpCI Nothing
  set0 <- fmap Vector.head $ Vk.allocateDescriptorSets vkLogical zero
    { Vk.descriptorPool = descPool
    , Vk.setLayouts = set0layout
    }

  withMVar (envSet0data env) \old@Buffer.Allocated{aBuffer} -> do
    let
      new = Storable.singleton Render.Set0
        { cameraVP = adjustScreen extent
        }
    _same <- Buffer.updateCoherent new old

    let
      writeSet0 = SomeStruct zero
        { Vk.dstSet          = set0
        , Vk.dstBinding      = 0
        , Vk.dstArrayElement = 0
        , Vk.descriptorCount = 1
        , Vk.descriptorType  = Vk.DESCRIPTOR_TYPE_UNIFORM_BUFFER
        , Vk.bufferInfo      = Vector.singleton set0bind0
        }
      set0bind0 = Vk.DescriptorBufferInfo
        { Vk.buffer = aBuffer
        , Vk.offset = 0
        , Vk.range  = Vk.WHOLE_SIZE
        }
    Vk.updateDescriptorSets vkLogical (pure writeSet0) mempty

  pure Render.ForSwapchain
    { fsSwapchain = swapchain
    -- , fsShadow = shadow
    , fsRender    = forward
    , fsDescPool  = descPool
    -- , fsShadowDescPool  = descPool
    , fsPipelines = pipelines
    , fsSet0      = set0
    }
  where
    extent = Swapchain.extent swapchain
    -- swapLength = fromIntegral . Vector.length $ Swapchain.views swapchain

    dpCI = descPoolCI MAX_SETS
      [ ( Vk.DESCRIPTOR_TYPE_UNIFORM_BUFFER
        , uniformBuffers
        )
      ]
      where
        uniformBuffers = 1 -- 1 shared camera

pattern MAX_SETS :: (Eq a, Num a) => a
pattern MAX_SETS = 1

pattern INFLIGHT_FRAMES :: (Eq a, Num a) => a
pattern INFLIGHT_FRAMES = 2

descPoolCI
  :: Word32
  -> [(Vk.DescriptorType, Word32)]
  -> Vk.DescriptorPoolCreateInfo '[]
descPoolCI maxSets sizes = zero
  { Vk.maxSets   = maxSets
  , Vk.poolSizes = Vector.fromList $ map (uncurry Vk.DescriptorPoolSize) sizes
  }

destroyForSwapchain :: Render.ForSwapchain -> RIO App ()
destroyForSwapchain Render.ForSwapchain{..} = do
  context <- asks appContext

  Vk.destroyDescriptorPool (Context.getVkLogical context) fsDescPool Nothing

  Pipeline.destroy context $ Render.solid fsPipelines
  Pipeline.destroy context $ Render.line fsPipelines

  Forward.destroyMsaa context fsRender

-- | Correct for screen aspect ratio so the squares will be drawn square.
adjustScreen :: Vk.Extent2D -> Linear.M44 Float
adjustScreen extent = Linear.V4
  (Linear.V4 x 0 0 0)
  (Linear.V4 0 y 0 0)
  (Linear.V4 0 0 z 0)
  (Linear.V4 0 0 0 1)
  where
    x = s / width
    y = s / height
    z = 1.0

    s = 2 * min (width / sx) (height / sy)
      where
        Linear.V2 sx sy = size

    (width, height) = (fromIntegral iWidth, fromIntegral iHeight)
      where
        Vk.Extent2D{width=iWidth, height=iHeight} = extent

size :: Num a => Linear.V2 a
size = Linear.V2 220 360
