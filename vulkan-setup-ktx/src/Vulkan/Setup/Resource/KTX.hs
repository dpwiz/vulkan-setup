module Vulkan.Setup.Resource.KTX
  ( createKtx1
  ) where

import Control.Exception (bracket, throwIO)
import Control.Monad (unless, when)
import Control.Monad.IO.Class (MonadIO(..))
import Data.ByteString.Unsafe (unsafeUseAsCStringLen)
import Vulkan.CStruct.Extends (SomeStruct(..))
import Vulkan.Setup.Buffer.Command (oneshot_)
import Vulkan.Setup.Context (Context)
import Vulkan.Setup.Image (AllocatedImage(..))
import Vulkan.Setup.Render.Texture (Texture(..), TextureLayers(..))
import Vulkan.Zero (zero)

import qualified Codec.Ktx as Ktx1
import qualified Foreign
import qualified Data.Text as Text
import qualified Data.Vector as Vector
import qualified Vulkan.Core10 as Vk
import qualified Vulkan.Setup.Buffer.Image as Image
import qualified Vulkan.Setup.Physical as Physical
import qualified Vulkan.Setup.Context as Context
import qualified Vulkan.Setup.Render.Texture as Texture
import qualified Vulkan.Utils.FromGL as FromGL
import qualified VulkanMemoryAllocator as VMA
import qualified Vulkan.Extensions.VK_KHR_surface as Khr

createKtx1
  :: forall a w io . (TextureLayers a, MonadIO io)
  => Context w
  -> Vk.CommandPool
  -> FilePath
  -> io (Texture a)
createKtx1 ctx pool path =
  liftIO (Ktx1.fromFile path) >>= \case
    Left (offset, err) ->
      liftIO . throwIO $ Texture.LoadError offset (Text.pack err)
    Right Ktx1.Ktx{header=Ktx1.Header{..}, images} -> do
      let
        Khr.SurfaceFormatKHR{format=surfaceFormat} = Physical.surfaceFormat $ Context.physical ctx
        srgbSurface = elem surfaceFormat
          [ Vk.FORMAT_B8G8R8A8_SRGB
          , Vk.FORMAT_R8G8B8A8_SRGB
          ]

      -- XXX: https://github.com/KhronosGroup/KTX-Software/blob/bf849b7f/lib/vk_format.h#L676
      format <- case glInternalFormat of
        -- XXX: [AMD] force SRGB if display surface is SRGB
        36492 | srgbSurface ->
          pure Vk.FORMAT_BC7_SRGB_BLOCK
        other ->
          case FromGL.internalFormat other of
            Nothing ->
              error $ "Unexpected glInternalFormat: " <> show glInternalFormat -- TODO: throwIo
            Just fmt ->
              -- XXX: going in blind
              pure fmt

      -- XXX: https://github.com/KhronosGroup/KTX-Software/blob/bf849b7f/lib/vkloader.c#L552
      let
        extent = Vk.Extent3D pixelWidth pixelHeight (max 1 pixelDepth)
        arrayLayers = max 1 numberOfArrayElements

      -- TODO: basisu can encode movies as arrays, this could be handy
      unless (arrayLayers == 1) $
        liftIO . throwIO $ Texture.ArrayError 1 arrayLayers

      let
        mipSizes = fmap ((*) numberOfFaces . Ktx1.imageSize) images
        offsets = Vector.scanl' (+) 0 mipSizes
        totalSize = Vector.last offsets

      -- traceShowM (path, format, (Vector.length images, arrayLayers, numberOfFaces))
      -- traceShowM (offsets, Vector.length offsets, totalSize, extent)

      (image, allocation, _info) <- VMA.createImage
        vma
        (Texture.imageCI format extent numberOfMipmapLevels numberOfFaces)
        Texture.imageAllocationCI

      Image.transitionImageLayout
        ctx
        pool
        image
        numberOfMipmapLevels
        numberOfFaces -- XXX: arrayLayers is always 0 for now
        format
        Vk.IMAGE_LAYOUT_UNDEFINED
        Vk.IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL

      when (numberOfFaces /= textureLayers @a) $
        liftIO . throwIO $ Texture.LayerError (textureLayers @a) numberOfFaces

      liftIO $
        VMA.withBuffer vma (Texture.stageBufferCI totalSize) Texture.stageAllocationCI bracket \(staging, stage, stageInfo) -> do
          Vector.forM_ (Vector.zip offsets images) \(offset, Ktx1.MipLevel{imageSize, arrayElements}) ->
            Vector.forM_ arrayElements \Ktx1.ArrayElement{faces} -> do
              let ixFaces = Vector.zip (Vector.fromList [0..]) faces
              Vector.forM_ ixFaces \(faceIx, Ktx1.Face{zSlices}) ->
                Vector.forM_ zSlices \Ktx1.ZSlice{block} ->
                  unsafeUseAsCStringLen block \(pixelsPtr, pixelBytes) -> do
                    let blockOffset = offset + faceIx * imageSize
                    let sectionPtr = Foreign.plusPtr (VMA.mappedData stageInfo) (fromIntegral blockOffset)
                    -- traceM $ "Poking z-slice of face " <> tshow (faceIx, faceIx * imageSize)
                    -- traceShowM (offset, imageSize, sectionPtr, pixelBytes)
                    Foreign.copyBytes sectionPtr (Foreign.castPtr pixelsPtr) pixelBytes

          VMA.flushAllocation vma stage 0 Vk.WHOLE_SIZE

          Image.copyBufferToImage ctx pool staging image extent numberOfMipmapLevels numberOfFaces -- XXX: arrayLayers is always 0 for now

      liftIO $ oneshot_ ctx pool \cmd ->
        cmdBarrier cmd Vk.PIPELINE_STAGE_FRAGMENT_SHADER_BIT (barrierBase image)
          { Vk.subresourceRange = Texture.subrColor0 numberOfMipmapLevels numberOfFaces -- XXX: arrayLayers is always 0 for now
          , Vk.oldLayout        = Vk.IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
          , Vk.newLayout        = Vk.IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
          , Vk.srcAccessMask    = Vk.ACCESS_TRANSFER_WRITE_BIT
          , Vk.dstAccessMask    = Vk.ACCESS_SHADER_READ_BIT
          }

      imageView <- Texture.createImageView
        ctx
        image
        format
        numberOfMipmapLevels
        numberOfFaces

      let
        allocatedImage = AllocatedImage
          { aiAllocation = allocation
          , aiImage      = image
          , aiImageView  = imageView
          }
      pure Texture
        { tFormat         = format
        , tMipLevels      = numberOfMipmapLevels
        , tFaces          = numberOfFaces
        , tAllocatedImage = allocatedImage
        }
  where
    vma = Context.getVkAllocator ctx

    -- XXX: copypasta from CubeMap extended to multiple mipLevels
    cmdBarrier buf dstStage barrier = Vk.cmdPipelineBarrier
      buf
      Vk.PIPELINE_STAGE_TRANSFER_BIT
      dstStage
      zero
      mempty
      mempty
      (Vector.singleton $ SomeStruct barrier)

    barrierBase image = zero
      { Vk.srcQueueFamilyIndex = Vk.QUEUE_FAMILY_IGNORED
      , Vk.dstQueueFamilyIndex = Vk.QUEUE_FAMILY_IGNORED
      , Vk.image               = image
      }
